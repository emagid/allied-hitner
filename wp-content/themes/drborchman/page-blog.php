<?php get_header('blog'); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main site-blog" role="main">
			<h1 class="blog-title">In The News</h1>
 			<?php
	  			$args = array(
	    		'post_type' => 'blog',
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?>

		<div class="blogSection" id="<?php the_field('title'); ?>">
<!--
	 		<div id="blogImage">
	 			<img src="<?php the_field('image'); ?>" />
	 		</div>
-->
	 		<div id="blogText">
	 			<h1><?php the_field('title'); ?></h1>

<!--	 			<h6><//?php echo get_the_date( get_option('date_format') ); ?></h6>-->
				<p><?php the_field('post'); ?></p>
<!--				<h5>... <a href="<//?php the_permalink(); ?>" class="read-more">READ MORE</a></h5>-->
				
	 		</div>
		</div>

		<?php
			}
				}
			else {
			echo 'No Blog Posts Found';
			}
		?>

		</main><!-- .site-main -->
	</div><!-- .content-area -->

<//?php get_sidebar(); ?>
<//?php get_sidebar('blog'); ?>
<?php get_footer(); ?>
