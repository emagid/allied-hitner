<?php ?>
<!DOCTYPE html>
<html>
<head>

	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="stylesheet" href="<?php bloginfo( 'style.css' ); ?>" type="text/css">
	<link rel="stylesheet" href="<?php bloginfo( 'lightbox.css' ); ?>" type="text/css">
  	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet">
  	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="wp-content/themes/drborchman/js/lightbox.js" type="text/javascript"></script>
	<script src="wp-content/themes/drborchman/js/scripts.js" type="text/javascript"></script>
	<?php wp_head(); ?>
</head>

<header>

 	<div id="topBar">
 		<div class="container">
	 		<div id="topBarLogo">
	 			<a href="/"><img src="<?php bloginfo('template_directory'); ?>/images/allied-logo.png"></a>
	 		</div>
	 		<div id="topBarText">
	 		<div id="topBarText">
			<?php
				if(is_active_sidebar('practice-name')){
					dynamic_sidebar('practice-name');
				}
			?>

	 		</div>
	 		</div>
	 		<div id="topBarSocial">
	 			<a href="https://www.facebook.com/DrHitner/" target="blank"><img src="<?php bloginfo('template_directory'); ?>/images/facebook-icon.png"></a>
	 		</div>
	 		<div id="topBarContact">
	 			<?$id = 39;
			$post = get_post($id); 
			$content = $post->post_content;
			?>
			<h2><?=$content?></h2>
	 		</div>
	 		<div id="topBarPortal">
	 			<h3><?php echo do_shortcode("[links category_name=PatientPortal]"); ?></h3>
	 		</div>
 		</div>
 	</div>

 	<nav id="navBar">
 		<div class="container">
 			 <div id="navHome">
 				<a href="/"><img src="<?php bloginfo('template_directory'); ?>/images/home-icon.png"></a>
	 		</div>
	 		<?php wp_nav_menu( array( 'theme_location' => 'blogmenu' ) ); ?>
 			<ul class="navLinks"> 

 			</ul>	
 		</div>
 	</nav>
 </header>


<body <?php body_class(); ?>>
