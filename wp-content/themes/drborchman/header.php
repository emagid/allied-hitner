<?php ?>
<!DOCTYPE html>
<html>
<head>

	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="stylesheet" href="/wp-content/themes/drborchman/style.css" type="text/css">
	<link rel="stylesheet" href="/wp-content/themes/drborchman/lightbox.css" type="text/css">
  	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="/wp-content/themes/drborchman/js/lightbox.js" type="text/javascript"></script>
    
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.css"/>
    <link rel="stylesheet" type="text/css" href="    https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css"/>
    
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.js"></script>
	<script src="/wp-content/themes/drborchman/js/scripts.js" type="text/javascript"></script>

	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-89521380-1', 'auto');
	  ga('send', 'pageview');

	</script>
	<?php wp_head(); ?>
</head>

<header>

 	<div id="topBar">
 		<div class="container overlay_bg">
	 		<div id="topBarLogo">
	 			<a href="/"><img src="<?php bloginfo('template_directory'); ?>/images/allied-logo.png"></a>
	 		</div>
	 		<div id="topBarText">
			<?php
				if(is_active_sidebar('practice-name')){
					dynamic_sidebar('practice-name');
				}
			?>

	 		</div>
	 		<div id="topBarSocial">
	 			<a href="https://www.facebook.com/DrHitner/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/facebook-icon.png"></a>
	 		</div>
	 		<div id="topBarContact">
			<?php
				if(is_active_sidebar('practice-number')){
					dynamic_sidebar('practice-number');
				}
			?>
	 		</div>
	 		<div id="topBarPortal">
	 			<h3><?php echo do_shortcode("[links category_name=PatientPortal]"); ?></h3>
	 		</div>
 		</div>
 	</div>

 	<nav id="navBar">
 		<div class="container">
 			 <div id="navHome">
 				<a href="/"><img src="<?php bloginfo('template_directory'); ?>/images/home-icon.png"></a>
	 		</div>
	 		<?php 
		wp_nav_menu(array(
		"theme_location" => "primary",
    	"container_class"=>"container",
    	"menu_class" => "navLinks"
    )); ?>
 			<ul class="navLinks"> 
				
 			</ul>	
 		</div>
 	</nav>
 </header>


<body <?php body_class(); ?>>
