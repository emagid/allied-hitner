<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
require_once SABRES_PLUGIN_DIR.'/_inc/settings.php';
require_once SABRES_PLUGIN_DIR.'/library/wp.php';
class Check_Sabres_Config {
  const TEMPLATE_VERSION=1;

  public static function check() {
    $settings=SbrSettings::instance();
    $config_file_path=SABRES_PLUGIN_DIR.'/sabres-config.php';
    $critical_site_parameters_changed=self::check_critical_site_parameters($settings);
    if ($critical_site_parameters_changed) {      
       @unlink($config_file_path);
       if (!$settings->should_trigger_activation()) {
         $settings->reset(array('trigger_hourly_cron_now'=>'true'));
         die("Sabres has detected that a critical website identifier has changed. If you have moved this website or changed its URL Sabres plugin will attempt to reconnect with Sabres cloud backend. If the URL was changed you will need to add the website again in customer portal dashboard");
       }
     }
    if ($settings->sabres_config_error!=='') {
      return;
    }
    $file_exists=@include_once $config_file_path;

    $update_plugin_url=false;
    if ($file_exists) {
      $curr_template_version=0;
      if (method_exists('Sabres_Config','get_version'))
        $curr_template_version=Sabres_Config::get_version();
      if (self::TEMPLATE_VERSION>$curr_template_version || Sabres_Config::any_value_changed()) {
        if (Sabres_Config::is_plugin_url_changed()) {
          $update_plugin_url=true;
        }
        if (!@unlink($config_file_path)) {
          $err = error_get_last();
          $settings->sabres_config_error=$err['message'];
          return;
        }
        Sabres_Config::reset();
        $config_deleted=true;
      }
    }
    else
        $update_plugin_url=true;
    if ($config_deleted || !$file_exists) {
      $template=file_get_contents(SABRES_PLUGIN_DIR.'/sabres-config-template.php');
      $result=Sabres_WP::instance()->file_put_contents_safe($config_file_path,'e3e74741-ff42-488c-870f-293ab212ab4b',sprintf($template,ABSPATH,SABRES_PLUGIN_DIR,SABRES_PLUGIN_BASE_NAME,SBS_PLUGIN_URL,SBS_MAIN_PLUGIN_FILE,WP_CONTENT_DIR));
      if (is_wp_error($result)) {
        $settings->sabres_config_error=$result->get_error_message();
      }
      else {
        @include_once $config_file_path;
      }
    }
    if ($update_plugin_url && !$settings->should_trigger_activation()) {
      if ($settings->update_plugin_url!='true') {
        $settings->update_plugin_url='true';
        $settings->trigger_hourly_cron_now='true';
      }
    }
  }

  public static function check_critical_site_parameters($settings) {
    $result=false;
    $prevValue=$settings->ABSPATH;
    $currValue=ABSPATH;
    if ($prevValue==='')
      $settings->ABSPATH=$currValue;
    elseif ($prevValue!=$currValue)
      $result=true;
    $prevValue=$settings->site_url;
    $currValue=get_site_url();
    if ($prevValue==='')
      $settings->site_url=$currValue;
    elseif ($prevValue!=$currValue)
      $result=true;
    $prevValue=$settings->server_uname;
    $currValue=php_uname('s').' '.php_uname('n').' '.php_uname('r').' '.php_uname('m');
    if ($prevValue==='')
      $settings->server_uname=$currValue;
    elseif ($prevValue!=$currValue)
      $result=true;

    return $result;

  }

}
Check_Sabres_Config::check();
