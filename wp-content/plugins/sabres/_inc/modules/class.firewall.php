<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}

require_once 'class.blocked-range.php';
require_once 'class.firewall.check.url.php';
require_once 'class.firewall.check.rpc.php';
require_once 'class.firewall.check.sqli.php';
require_once 'class.firewall.check.xss.php';

/*
 * Copyright 2016 Sabres Security Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


if ( !class_exists( 'SBS_Firewall' ) ) {
     require_once SABRES_PLUGIN_DIR.'/_inc/class.singleton.php';

    /**
     * The Sabres Firewall Class
     *
     * @author Ariel Carmely - Sabres Security Team
     * @package Sabres_Security_Plugin
     * @since 1.0.0
     */
    final class SBS_Firewall extends SBS_Singleton {

        private $blacklist_ranges_filepath;
        private $blacklist_ranges;
        private $settings;

        private $check_url;
        private $check_rpc;
        private $check_sqli;
        private $check_xss;

        protected static $instance;

        protected function __construct() {
            $this->check_url = new SBS_Firewall_Check_Url();
            $this->check_rpc = new SBS_Firewall_Check_RPC();
            $this->check_sqli = new SBS_Firewall_Check_Sqli();
            $this->check_xss = new SBS_Firewall_Check_XSS();
        }

        public function init( $settings = null ) {
            $this->settings = (object) array_change_key_case( $settings, CASE_LOWER );
            $this->check_url->init( $settings );
            $this->check_rpc->init( $settings );
            $this->check_sqli->init( $settings );
            $this->check_xss->init( $settings );
        }

        public function process_request() {            
            $ip_array = (array) explode( ',', SBS_Net::get_real_ip_address() );
            $ip_address = trim( $ip_array[0] );
            $ip = SBS_Net::ip2long( $ip_address );
            $unique_id = Sabres::$unique_id;


            if ( isset( $this->check_rpc ) && $this->check_rpc->check_request() ) {
                $this->do_action( $ip_address, 'block', 'XMLRPC;' );

                return;
            }

            if ( isset( $this->check_sqli ) && $this->check_sqli->check_request() ) {
                $this->do_action( $ip_address, 'block', 'SQLI;' );

                return;
            }

            if ( isset( $this->check_xss ) && $this->check_xss->check_request() ) {
                $this->do_action( $ip_address, 'block', 'XSS;' );

                return;
            }

            if ( isset( $this->check_url ) && $this->check_url->check_request() ) {
                $this->do_action( $ip_address, 'block', 'URL;' );

                return;
            }

            if ( !$ip ) $ip = 1;

            $entries = $this->find_entries( $ip, $unique_id );
            $found_entry = null;

            if ( !empty( $entries) ) {
                foreach ( $entries as $entry ) {
                    if ( !empty( $entry->expiry ) ) {
                        $current_time = SBS_DB::get_current_time();
                        $expiryTime = strtotime( "+$entry->expiry seconds", strtotime( $entry->created_at ) );

                        if ( $current_time >= $expiryTime ) {
                            switch( strtoupper( $entry->type ) ) {
                                case 'CU':
                                    $res = $this->delete_custom_entry( $ip );
                                    break;
                                case 'CK':
                                    $res = $this->delete_cookie_entry( $unique_id );
                                    break;
                                case 'CY':
                                    $res = $this->delete_country_entry( $ip );
                                    break;
                            }

                            if ( $res !== false ) {
                                continue;
                            }
                        }
                    }

                    if ( !isset( $target_entry ) ) {
                        $found_entry = $entry;
                        break;
                    }
                }
            }

            $blocked = null;
            $allowed = null;

            if ( isset( $this->settings->country_mode ) && $this->settings->country_mode === 'block_all' ) {
                $blocked = true;
            }

            if ( isset( $found_entry ) ) {
                $allowed = $this->do_action( $ip_address, $found_entry->do_action, $found_entry->description );
            }

            if ( isset( $blocked ) && $blocked && !( isset( $allowed ) && $allowed ) ) {
                $this->do_action( $ip_address, 'block', 'CY_MODE;' );
            }
        }

        private function do_action( $ip_address, $action, $description ) {
            if ( $action == 'allow' ){
                Sabres::$request_data['firewall-action'] = $this->getActionCode( $action );
                Sabres::$request_data['firewall-desc'] = $this->getActionCode( $description );

                return true;
            } else {
                Sabres::$logger->log( 'info', 'mod-firewall-block', "$ip_address blocked", array(
                    'ip' => $ip_address,
                    'action' => $action
                ) );
                require_once SABRES_PLUGIN_DIR.'/library/cache.php';
                require_once SABRES_PLUGIN_DIR.'/library/net.php';
                SBS_Cache::disable_cache();
                SBS_Net::disable_cache();

                switch ( $action ) {
                    case 'block':
                        $this->dispatch_request( $action, $description );

                        $this->do_block( "$ip_address is blocked" );
                        break;
                    case 'captcha':
                        $captcha = 1;
                        $recurring = in_array( 'rcg', array_map( 'strtolower', explode( ';', $description ) ) );

                        if ( !empty( $_SESSION['sbs_firewall_captcha_phrase'] ) ) {
                            if ( !empty($_POST['sbs_firewall_captcha_refresh'] ) ) {
                                $captcha = 1;

                                unset( $_SESSION['sbs_firewall_captcha_phrase'] );
                            }
                            else if ( !empty( $_POST['sbs_firewall_captcha_phrase'] ) ) {
                                $captcha_phrase = $_POST['sbs_firewall_captcha_phrase'];
                                $saved_captcha_phrase = $_SESSION['sbs_firewall_captcha_phrase'];

                                if ( strcasecmp( $captcha_phrase, $saved_captcha_phrase ) == 0 ) {
                                    if ( $recurring || $this->delete_custom_entry( SBS_Net::ip2long( $ip_address ) ) !== false ) {
                                        $captcha = 0;
                                    }

                                    Sabres::$request_data['firewall-action'] = 'CS';
                                    Sabres::$request_data['firewall-desc'] = $description;
                                }

                                unset( $_SESSION['sbs_firewall_captcha_phrase'] );
                            }
                        }

                        if ( $captcha ) {
                            $this->dispatch_request( $action, $description );

                            $this->do_captcha();
                        }
                        break;
                    case 'redirect':
                        $this->dispatch_request( $action, $description );

                        global $wp_query;

                        $wp_query->set_404();

                        header('HTML/1.1 404 Not Found', true, 404);
                        header('Status: 404 Not Found');
                        @include(get_template_directory () . '/404.php');
                        break;
                    case 'captcha':
                        break;
                }
            }
        }

        private function do_captcha() {
            $captcha = SBS_Captcha::get_captcha();

            if ( !empty( $captcha['keyphrase'] ) && !empty( $captcha['captcha'] ) ) {
                $_SESSION['sbs_firewall_captcha_phrase'] = $captcha['keyphrase'];

                $captcha_data = $captcha['captcha'];
                Sabres::init_firewall_response();
                if ( !empty( $captcha_data ) ) {
                    require_once __DIR__ . '/views/firewall.captcha.php';
                }
            }
            exit();
        }

        private function do_block( $message ) {
            Sabres::init_firewall_response();

            header('Status: 503 Service Unavailable');

            require_once __DIR__ . '/views/firewall.block.php';

            die();
        }

        private function delete_country_entry( $ip ) {
            global $wpdb;

            $sql = "DELETE FROM " . $wpdb->prefix . "sbs_firewall_countries where $ip >= " . $wpdb->prefix . "sbs_firewall_countries.from_ip AND $ip <= " . $wpdb->prefix . "sbs_firewall_countries.to_ip";

            return $wpdb->query( $sql );
        }

        private function delete_custom_entry( $from, $to = null ) {
            global $wpdb;

            if ( !$to ) {
                $to = $from;
            }

            $sql = "DELETE FROM " . $wpdb->prefix . "sbs_firewall_custom where $from >= " . $wpdb->prefix . "sbs_firewall_custom.from_ip AND $to <= " . $wpdb->prefix . "sbs_firewall_custom.to_ip";

            return $wpdb->query( $sql );
        }


        private function delete_cookie_entry( $unique_id ) {
            global $wpdb;

            $sql = "DELETE FROM " . $wpdb->prefix . "sbs_firewall_cookies where " . $wpdb->prefix . "sbs_firewall_cookies.unique_id = '$unique_id'";

            return $wpdb->query( $sql );
        }

        private function dispatch_request( $action, $description ) {
            Sabres::init_variables();
            Sabres::init_request_data();

            Sabres::dispatch_request( array(
                'firewall-action' => $this->getActionCode( $action ),
                'firewall-desc' => $description
            ));
        }

        private function getActionName( $code ) {
            switch ( strtoupper( $code ) ) {
                case 'A':
                    return 'allow';
                case 'B':
                    return 'block';
                case 'C':
                    return 'captcha';
                case 'R':
                    return 'redirect';
            }
        }

        private function getActionCode( $name ) {
            switch ( strtolower( $name ) ) {
                case 'allow':
                    return 'A';
                case 'block':
                    return 'B';
                case 'captcha':
                    return 'C';
                case 'redirect':
                    return 'R';
            }
        }

        public function add_country( $code, $action, $description, $expiry = null ) {
            // DB
            global $wpdb;

            return $wpdb->insert( $wpdb->prefix . 'sbs_firewall_countries',
                array(
                    'code'        => $code,
                    'do_action'      => $this->getActionName( $action ),
                    'description' => $description,
                    'expiry'      => !empty( $expiry ) ? $expiry : 'NULL'
                ),
                array (
                    '%s',
                    '%s',
                    '%s',
                    '%d'
                )
            );
        }

        public function add_countries( $data ) {
            global $wpdb;
            $wpdb->query( 'TRUNCATE TABLE ' . $wpdb->prefix . 'sbs_firewall_countries' );

            if ( !empty($data) ) {
                $wpdb->query( 'BEGIN', $wpdb->dbh );

                foreach( $data as $item ) {
                    $this->add_country( $item['code'], $item['action'], $item['desc'], !empty( $item['expiry'] ) ? $item['expiry'] : null );
                }

                if ( isset( $error ) ) {
                    $wpdb->query( 'ROLLBACK', $wpdb->dbh );
                } else {
                    $wpdb->query( 'COMMIT', $wpdb->dbh );
                }
            }
        }

        public function add_custom( $from, $to, $action, $description, $expiry = null, $global = null ) {
            // DB
            global $wpdb;

            $this->delete_custom_entry( $from, $to );

            return $wpdb->insert( $wpdb->prefix . 'sbs_firewall_custom',
                array(
                    'from_ip'        => $from,
                    'to_ip'          => $to,
                    'do_action'      => $this->getActionName( $action ),
                    'description'    => $description,
                    'expiry'         => $expiry,
                    'global_rule'         => $global
                ),
                array (
                    '%d',
                    '%d',
                    '%s',
                    '%s',
                    '%d',
                    '%d'
                )
            );
        }

        public function add_custom_range( $data, $purge = null ) {
            global $wpdb;

            if ( !empty( $purge ) ) {
                $wpdb->query( 'DELETE FROM ' . $wpdb->prefix . 'sbs_firewall_custom WHERE ' . $wpdb->prefix . 'sbs_firewall_custom.global_rule = 0' );
            }

            if ( !empty( $data ) ) {
                $wpdb->query( 'BEGIN', $wpdb->dbh );

                foreach( $data as $item ) {
                    $expiry = null;
                    if ( !empty( $item['expiry'] ) ) {
                        $expiry = $item['expiry'];
                    }

                    if ($expiry == -1) {
                        $this->delete_custom_entry( $item['from'], $item['to'] );

                        continue;
                    }

                    $global = null;
                    if ( !empty( $item['global'] ) ) {
                        $global = $item['global'];
                    } else {
                        $global = false;
                    }

                    $this->add_custom( $item['from'], $item['to'], $item['action'], $item['desc'], $expiry, $global );
                }

                if ( isset( $error ) ) {
                    $wpdb->query( 'ROLLBACK', $wpdb->dbh );
                } else {
                    $wpdb->query( 'COMMIT', $wpdb->dbh );
                }
            }
        }

        public function add_unique_id( $unique_id, $action, $description, $expiry ) {
            // DB
            global $wpdb;

            $this->delete_cookie_entry( $unique_id );

            return $wpdb->insert( $wpdb->prefix . 'sbs_firewall_cookies',
                array(
                    'unique_id'        => $unique_id,
                    'do_action'      => $this->getActionName( $action ),
                    'description' => $description,
                    'expiry'      => $expiry,
                ),
                array (
                    '%s',
                    '%s',
                    '%s',
                    '%d'
                )
            );
        }

        public function add_unique_ids( $data ) {
            global $wpdb;

            $wpdb->query( 'TRUNCATE TABLE ' . $wpdb->prefix . 'sbs_firewall_cookies' );

            if ( !empty( $data ) ) {
                $wpdb->query( 'BEGIN', $wpdb->dbh );

                foreach( $data as $item ) {
                    $this->add_unique_id( $item['uniqueId'], $item['action'], $item['desc'], !empty( $item['expiry'] ) ? $item['expiry'] : null );
                }

                if ( isset( $error ) ) {
                    $wpdb->query( 'ROLLBACK', $wpdb->dbh );
                } else {
                    $wpdb->query( 'COMMIT', $wpdb->dbh );
                }
            }
        }

        private function find_entries( $ip = null, $unique_id = null ) {
            global $wpdb;
            // DB
            $query = '';

            if ( !empty( $unique_id ) ) {
                $query = "SELECT 'CK' as type, " . $wpdb->prefix . "sbs_firewall_cookies.do_action, " . $wpdb->prefix . "sbs_firewall_cookies.description, " . $wpdb->prefix . "sbs_firewall_cookies.expiry, " . $wpdb->prefix . "sbs_firewall_cookies.created_at FROM " . $wpdb->prefix . "sbs_firewall_cookies where " . $wpdb->prefix . "sbs_firewall_cookies.unique_id = '$unique_id'";
            }

            if ( !empty( $ip ) ) {
                $ip_address = long2ip( $ip );
                $country_code = SBS_IP::get_country_code( $ip_address );

                if ( $query != '' ) {
                    $query .= ' UNION ALL ';
                }
                $query .= "SELECT 'CU' as type, " . $wpdb->prefix . "sbs_firewall_custom.do_action, " . $wpdb->prefix . "sbs_firewall_custom.description, " . $wpdb->prefix . "sbs_firewall_custom.expiry, " . $wpdb->prefix . "sbs_firewall_custom.created_at FROM " . $wpdb->prefix . "sbs_firewall_custom where $ip >= " . $wpdb->prefix . "sbs_firewall_custom.from_ip AND $ip <= " . $wpdb->prefix . "sbs_firewall_custom.to_ip";

                if ( !empty( $country_code ) ) {
                    $query .= ' UNION ALL ';
                    $query .= "SELECT 'CY' as type, " . $wpdb->prefix . "sbs_firewall_countries.do_action, " . $wpdb->prefix . "sbs_firewall_countries.description, " . $wpdb->prefix . "sbs_firewall_countries.expiry, " . $wpdb->prefix . "sbs_firewall_countries.created_at FROM " . $wpdb->prefix . "sbs_firewall_countries where '$country_code' = " . $wpdb->prefix . "sbs_firewall_countries.code";
                }
            }

            if ( $query != '' ) {
                global $wpdb;
                return $wpdb->get_results( $query, OBJECT );
            }
        }

        private function store_blacklist_ranges() {
            if ( $this->blacklist_ranges_filepath != '' ) {
                if ( @file_put_contents( $this->blacklist_ranges_filepath, serialize( $this->blacklist_ranges ) ) ) {
                }
            }
        }

        public function block_range( $from, $to, $action, $expiry ) {

        }
    }
}
