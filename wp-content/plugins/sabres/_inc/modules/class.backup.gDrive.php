<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}

/*
 * Copyright 2016 Sabres Security Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


if ( !class_exists( 'SBS_Backup_G_Drive' ) ) {
    require_once SABRES_PLUGIN_DIR.'/library/fail.php';
    require_once SABRES_PLUGIN_DIR.'/library/vendor/autoload.php';
    /**
     * The Sabres Upload to GD Class
     *
     * @author Ariel Carmely - Sabres Security Team
     * @package Sabres_Security_Plugin
     * @since 1.0.0
     */
    final class SBS_Backup_G_Drive
    {
        private $settings;
        private $filename_with_out_ext;
        private $clientId;
        private $client_secret;
        protected static $instance;

        public function __construct() {
        }

        public function register_event_callback( $event_name, $callback ) {
            if ( !isset( $this->_event_callbacks[$event_name] ) ) {
                $this->_event_callbacks[$event_name] = array();
            }

            array_push( $this->_event_callbacks[$event_name], $callback );
        }

        private function event_trigger( $event_name, $args ) {
            if ( !empty( $this->_event_callbacks[$event_name] ) ) {
                $callbacks = $this->_event_callbacks[$event_name];

                foreach ( $callbacks as $callback ) {
                    if ( !empty( $args ) ) {
                        call_user_func_array( $callback, $args );
                    } else {
                        call_user_func( $callback );
                    }
                }
            }
        }

        public function init( $settings = null, $filename_with_out_ext, $clientId, $client_secret ) {
            if ( !empty( $settings ) ) {
                $this->settings = (object) array_change_key_case( $settings, CASE_LOWER );
            }
            $this->filename_with_out_ext = $filename_with_out_ext;
            $this->clientId = $clientId;
            $this->client_secret = $client_secret;
        }

        public function is_valid() {
            $is_valid = true;
            $fail_args=array('code'=>500,
                            'includeBacktrace'=>true);

            // Check vendor
            //include_once ( __DIR__ . '/../../library/vendor/google/apiclient/src/Google/autoload.php' );

            // Check if file exist
            $path_with_file_name_without_ext = realpath ( WP_CONTENT_DIR . "/sbs-backup/" . $this->filename_with_out_ext );

            $path_to_db_archive = $path_with_file_name_without_ext . ".sql.zip";
            if (!$path_to_db_archive)
            {
                $this->event_trigger( 'error', array(
                    'desc' => 'File db zip not found : ' . $path_with_file_name_without_ext . ".sql.zip"
                ) );
                $is_valid = false;
                $fail_args['message']='File db zip not found : ' . $path_with_file_name_without_ext . ".sql.zip";
                SBS_Fail::byeArr($fail_args);
            }

            $path_to_files_archive = $path_with_file_name_without_ext . ".zip";
            if (!$path_to_files_archive)
            {
                $this->event_trigger( 'error', array(
                    'desc' => 'Files zip not found : ' . $path_with_file_name_without_ext . ".zip"
                ) );
                $is_valid = false;
                $fail_args['message']='Files zip not found : ' . $path_with_file_name_without_ext . ".zip";
                SBS_Fail::byeArr($fail_args);
            }

            // Check if free space in google drive - in upload file, why should the client and the Service object's

            // Check execution time limit
//            if ( !ini_set( 'max_execution_time', '0' ) && !set_time_limit( 0 ) ) {
//                $is_valid = false;
//            }
//
//            // Check execution memory limit
//            if ( !ini_set( 'memory_limit', '-1' ) ) {
//                $is_valid = false;
//            }

            return $is_valid;
        }

        private function set_setting_run() {
            // Disable time limit
            ini_set( 'max_execution_time', '0' );
            set_time_limit( 0 );

            // Disable memory limit
            ini_set( 'memory_limit', '-1' );

            define( 'APPLICATION_NAME', 'Drive API Quickstart' );
            define( 'CREDENTIALS_PATH', __DIR__ . '/creds/' );
            define ( 'DRIVE_QS_PATH',CREDENTIALS_PATH . 'drive-api-quickstart.json' ); // TODO: save to DB
         //   define('CLIENT_SECRET_PATH', CREDENTIALS_PATH . 'client_secret.json' );
            define( 'SCOPES', implode(' ', array(
                    Google_Service_Drive::DRIVE_METADATA_READONLY,
                    Google_Service_Drive::DRIVE,
                    Google_Service_Drive::DRIVE_FILE,
                    Google_Service_Drive::DRIVE_METADATA,
                )
            ));
        }

        private function get_client() {
            try {
                $client = new Google_Client();
                $client->setApplicationName( APPLICATION_NAME );
                $client->setScopes( SCOPES );

                $client->setClientId( $this->clientId );
                $client->setClientSecret( $this->client_secret );

                // Set 'redirect uri'
                $client->setRedirectUri( "urn:ietf:wg:oauth:2.0:oob" );
                $client->setAccessType( 'offline' );

                 return $client;
            } catch ( \Exception $e ) {
                $this->event_trigger( 'error', array(
                    'desc' => $e->getMessage()
                ) );
            }
        }

        public function run($access_token) {
            if ( $this->is_valid() )
            {
                $this->set_setting_run();
                $client = $this->get_client();

                $client->setAccessToken($access_token);

                self::upload_file( $client ); //$this->filename_with_out_ext );
            }
        }


        private function get_file_ids( $paretntId, $service ) {
            $pageToken = NULL;
            $parameters = array();
            $childrenIds = array();
            if ( $pageToken ) {
                $parameters['pageToken'] = $pageToken;
            }
            $children = $service->children->listChildren( $paretntId, $parameters );

            foreach ( $children->getItems() as $child ) {
                array_push( $childrenIds, $child->getId() );
            }

            return $childrenIds;
        }

        // Fetch authCode from google to create access token
        public function fetch() {
            $this->set_setting_run();
            $drive_qs_path = DRIVE_QS_PATH;
            $client = $this->get_client();

            $authCode = '4/TQLpNllkqIRpbkB6qFYsMHZmT7pOHZA3r4lHZDAIQhU'; //$_GET['code'];

            try {
                $accessToken = $client->authenticate( $authCode );
            }
            catch( \Exception $e )
            {
                $this->event_trigger( 'error', array(
                    'desc' => $e->getMessage()
                ) );
            }

            // TODO : Save access token in DB instead file
            if( !file_exists( dirname( $drive_qs_path ) ) ) {
                if ( !mkdir( dirname($drive_qs_path) ) ) {
                    $this->event_trigger( 'error', array(
                        'desc' => "No permissions to create folder: dirname( $drive_qs_path )"
                    ) );

                    return;
                }
            }
            file_put_contents( $drive_qs_path, $accessToken );

            self::upload_file( $client, $accessToken, $drive_qs_path );
        }

        private function upload_file( $client ) {

            $service = new Google_Service_Drive( $client );

            $root_dir = realpath( ABSPATH );

            $path_with_file_name_without_ext = WP_CONTENT_DIR . "/sbs-backup/" . $this->filename_with_out_ext;
            $path_to_db_archive = $path_with_file_name_without_ext . ".sql.gz";
            $path_to_files_archive = $path_with_file_name_without_ext . ".zip";

            // Check free space in google drive
            $size_of_both = filesize ( $path_to_db_archive ) + filesize ( $path_to_files_archive );
            self::has_free_disk_google_drive ( $size_of_both, $service );

            $parentId = $this->insertFile( $client, $service, 'sbs_db_archive_' . date('Y_m_d_Hi', filectime ($path_to_db_archive)) . '.sql.zip', 'desc', null, "application/x-gzip", $path_to_db_archive );
            $this->insertFile( $client, $service, 'sbs_files_archive_' . date('Y_m_d_Hi', filectime ($path_to_files_archive)) . '.zip', 'desc', $parentId, "application/zip", $path_to_files_archive );
//            echo 'Backup DB done succesfully!';

            // Upload files                                      ParentId                  MimeType              Source_file
            //  insertFile($client, $service, 'title', 'desc','0B9eI6ktZFF3PdmRtV0hZTFIyOFE', "application/zip", 'C:\Apache24\htdocs\file3\test100.zip' );

            // Need to save file ids in DB- for restore
          //  $paretntIds = self::get_file_ids ( $paretntId, $service );

        }

        // For restore
        function download_file ( $service, $fileId, $save_to_with_fileName ) {
          //  $fileId = '0B9eI6ktZFF3Pd1lxRUEyRnVnZm8';
            $file = $service->files->get ( $fileId );
            $file_url = $file->getWebContentLink();


            set_time_limit( 0 );
            $file = file_get_contents ( $file_url );
            file_put_contents ( $save_to_with_fileName, $file );
            exit();
        }

        function insertFile( $client, $service, $title, $description, $parentId, $mimeType, $filename ) {
            // Create file
            $file = new Google_Service_Drive_DriveFile();
            $file->setName($title);
            $file->setDescription( $description );
            $file->setMimeType( $mimeType );


            // Set the parent folder.
            if ( !is_null( $parentId ) ) {
                $file->setParents( [$parentId] );
            } else {

                $sabres_folder_id = $this->get_sabres_folder_id( $service );
                $parentId = $this->get_website_folder_id( $service, $sabres_folder_id );



                $file->setParents( [$parentId] );

                //$parent = new Google_Service_Drive_ParentReference();
//                $parent->setId( $folder->id );
//                $file->setParents( array( $parent ) );
//                $parentId = $parent->id;
//
//                // Set the parent folder.
//                $parent = new Google_Service_Drive_ParentReference();
//                $parent->setId( $parentId );
//                $file->setParents( array( $parent ) );
            }

            try {
                $chunkSizeBytes = 1 * 1024 * 1024;
                $client->setDefer( true );

                $createdFile = $service->files->create( $file );

                $media = new Google_Http_MediaFileUpload(
                    $client,
                    $createdFile,
                    $mimeType,
                    null,
                    true,
                    $chunkSizeBytes
                );
                $media->setFileSize( filesize( $filename ) );

                $status = false;
                $handle = fopen( $filename, "rb" );
                while ( !$status && !feof( $handle )) {
                    $chunk = fread( $handle, $chunkSizeBytes );
                    $status = $media->nextChunk( $chunk ); //$createdFile, $chunk);
                }

                fclose( $handle );

                // Uncomment the following line to print the File ID
                // print 'File ID: %s' % $createdFile->getId();
               // return $createdFile;
                return $parentId;
            } catch (\Exception $e) {
                $this->event_trigger( 'error', array(
                    'desc' => $e->getMessage()
                ) );
            }
            $client->setDefer( false );
        }

        function has_free_disk_google_drive( $size_of_file, $service ) {
            try {
                $about = $service->about->get();
                $free_space = $about->getQuotaBytesTotal() - $about->getQuotaBytesUsed();

                if ( !( $free_space > 0 && $free_space > $size_of_file ) )
                {
                    $this->event_trigger( 'error', array(
                        'desc' => 'Not enough free space'
                    ) );
                }
            } catch ( \Exception $e ) {
                $this->event_trigger( 'error', array(
                    'desc' => "An error occurred: " . $e->getMessage()
                ) );
            }
        }

        protected function get_sabres_folder_id($service) {
            $response = $service->files->listFiles(array(
                'q' => "mimeType='application/vnd.google-apps.folder' AND name='Sabres' AND trashed = false",
                'spaces' => 'drive',
                'fields' => 'files(id, name, trashed)',
            ));

            $folders = $response->files;
            if (!empty($folders)) {
                $folder = $folders[0];

                $parentId = $folder->id;
            }

            if ( empty( $parentId ) ) {
                // Create folder
                $fileMetadata = new Google_Service_Drive_DriveFile(array(
                    'name' => 'Sabres',
                    'mimeType' => 'application/vnd.google-apps.folder'));
                $folder = $service->files->create($fileMetadata, array(
                        'fields' => 'id'
                    )
                );
            }

            return $folder->id;
        }

        protected function get_website_folder_id($service, $sabres_folder_id) {
            $sitename = get_option( 'blogname' );

            $response = $service->files->listFiles(array(
                'q' => "mimeType='application/vnd.google-apps.folder' AND name='{$sitename}' AND '{$sabres_folder_id}' in parents AND trashed = false",
                'spaces' => 'drive',
                'fields' => 'files(id, name, trashed)',
            ));

            $folders = $response->files;
            if (!empty($folders)) {
                $folder = $folders[0];

                $parentId = $folder->id;
            }

            if ( empty( $parentId ) ) {
                // Create folder
                $fileMetadata = new Google_Service_Drive_DriveFile(array(
                    'name' => $sitename,
                    'mimeType' => 'application/vnd.google-apps.folder'));
                $fileMetadata->setParents([$sabres_folder_id]);
                $folder = $service->files->create($fileMetadata, array(
                        'fields' => 'id'
                    )
                );
            }

            return $folder->id;
        }
    }
}
