<?php

require_once SABRES_PLUGIN_DIR . '/library/fail.php';
require_once SABRES_PLUGIN_DIR . '/_inc/sbr_utils.php';

class Core_Inventory {
    public function execute($rpc_data) {

      $files = null;

      if ( !empty( $rpc_data['files'] ) ) {
          $files = $rpc_data['files'];
      }

      $res = json_encode( $this->get_core( $files ) );

      echo $res;
    }


    public function get_core( $files = null )
    {
        require_once ABSPATH.'/wp-load.php';
        global $wp_version;

        $ret = array(
            'version' => $wp_version,
            'files' => array()
        );

        $get_files = ( isset( $files ) && strcasecmp( trim( $files ), 'true' ) == 0 );

        if ( $get_files ) {
            $files_inv = SbrUtils::get_files( rtrim( ABSPATH, '/\\' ) );
            $files_inv = array_merge( $files_inv, SbrUtils::get_files( ABSPATH . 'wp-admin', true ) );
            $files_inv = array_merge( $files_inv, SbrUtils::get_files( ABSPATH . 'wp-includes', true ) );

            list( $files_inv, $failed_files) = SbrUtils::exclude_no_readable( $files_inv );

            if ( count( $failed_files ) ) {
                $logger = SBS_Logger::getInstance();
                $logger->log( 'warning', "RPC", "Core inventory", implode( ", ", $failed_files ) );
            }

            foreach ( $files_inv as $file ) {
                if ( !is_dir( $file ) ) {
                    $file_name = ltrim( str_replace( rtrim( ABSPATH, '/\\' ), '', $file ), '/\\' );
                    $file_name = str_replace( '\\', '/', $file_name );

                    $ret['files'][] = array(
                        'fullPath' => $file_name,
                        'signature' => @md5_file( $file )
                    );
                }
            }
        }

        return $ret;
    }



}
