<?php

require_once SABRES_PLUGIN_DIR . '/library/fail.php';
require_once SABRES_PLUGIN_DIR . '/modules/scanner.php';
require_once SABRES_PLUGIN_DIR.'/_inc/settings.php';

class Security_Scan {
    public function execute($rpc_data) {
      $scanner = SBS_Scanner::getInstance();
      $settings=SbrSettings::instance();
      $scanner->init( $settings->get_settings( 'mod_scanner' ), 'security' );
      $scanner->run();

      $scan = $scanner->get_current_scan();

      if ( isset( $scan ) ) {
          $scan_items = array();

          if ( !empty( $scan->items ) ) {
              $scan_items = array_map( function ( $item ) {
                  unset( $item->ID );
                  unset( $item->parent_id );

                  return $item;
              }, $scan->items );
          }

          $res = json_encode( array_values( $scan_items ) );
          echo $res;
      }

    }

}
