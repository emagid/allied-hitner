<?php

require_once SABRES_PLUGIN_DIR . '/library/fail.php';
require_once SABRES_PLUGIN_DIR.'/_inc/settings.php';

class Activate_Plugin {

  public function execute($rpc_data) {
    $settings=SbrSettings::instance();
    if ( empty( $rpc_data['token'] ) ) {
      SBS_Fail::byeArr(array( 'message'=>"RPC: Missing token parameter",
                              'code'=>400
                             ));
    }

    if ( $settings->token == '' ) {
       SBS_Fail::byeArr(array( 'message'=>"RPC: Missing token setting",
                              'code'=>500
                             ));
    }

    if ( $settings->token != $rpc_data['token'] ) {
      SBS_Fail::byeArr(array( 'message'=>"RPC: Token parameter differs from token setting",
                             'code'=>401
                            ));
    }

    if ( empty( $rpc_data['settings'] ) ) {
      SBS_Fail::byeArr(array( 'message'=>"RPC: Missing settings parameter",
                             'code'=>400
                            ));
    }

    require_once 'set_settings.php';
    $instance=new Set_Settings();
    $res=$instance->save(urldecode( $rpc_data['settings'] ));
    echo $res;
  }
}
