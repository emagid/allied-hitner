<?php

require_once SABRES_PLUGIN_DIR . '/library/fail.php';
require_once ABSPATH.'/wp-load.php';
require_once ( ABSPATH . '/wp-admin/includes/admin.php' );

class WP_Is_Plugin_Active {
  public function execute($rpc_data) {
    $result=is_plugin_active(SABRES_PLUGIN_BASE_NAME);
    echo var_export($result,true);
  }

}
