<?php

require_once SABRES_PLUGIN_DIR . '/library/fail.php';
require_once ABSPATH . '/wp-load.php';

class Themes_Inventory {
    public function execute($rpc_data) {
      $files = null;
      if ( !empty( $rpc_data['files'] ) ) {
          $files = $rpc_data['files'];
      }

      $res = json_encode( $this->get_themes( $files ) );
      echo $res;
    }


    public function get_themes( $files = null )
    {
        $getFiles = ( isset( $files ) && strcasecmp( trim( $files ), 'true' ) == 0 );
        $themes = array();
        $themesObjects = wp_get_themes();
        $props = array( 'Name', 'ThemeURI', 'Description', 'Author', 'AuthorURI', 'Version', 'Template', 'Status', 'Tags', 'TextDomain' );
        foreach ( $themesObjects as $themeRoot => $themeObj ) {
            $theme = array();
            foreach ( $props as $property ) {
                $theme[$property] = $themeObj->get( $property );
            }
            if ( $getFiles ) {
                $files = $themeObj->get_files( null, -1 );

                list( $files, $failed_files) = SbrUtils::exclude_no_readable( $files );

                if ( count( $failed_files ) ) {
                    $logger = SBS_Logger::getInstance();
                    $logger->log( 'warning', "RPC", "Themes inventory", implode( ", ", $failed_files ) );
                }

                $theme['Files'] = $files;
            }
            $themes[$themeRoot] = $theme;
        }

        return array( 'current' => get_stylesheet(), 'themes' => $themes );
    }

}
