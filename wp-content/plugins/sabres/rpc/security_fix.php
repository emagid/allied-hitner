<?php

require_once SABRES_PLUGIN_DIR . '/library/fail.php';
require_once SABRES_PLUGIN_DIR . '/modules/scanner.php';
require_once SABRES_PLUGIN_DIR.'/_inc/settings.php';

class Security_Fix {
    public function execute($rpc_data) {
      $scanner = SBS_Scanner::getInstance();
      $settings=SbrSettings::instance();
      $scanner->init( $settings->get_settings( 'mod_scanner' ), 'security' );

      $res = json_encode( $scanner->fix_issue( $rpc_data['code'], $rpc_data['uniqueId'] ) );
      echo $res;
    }

}
