<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}

/*
 * Copyright 2016 Sabres Security Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


if ( !class_exists( 'SBS_IP' ) ) {

    /**
     * The Sabres IP Class
     *
     * @author Sabres Security inc
     * @package Sabres_Security_Plugin
     * @since 1.0.0
     */
    abstract class SBS_IP {

        public static function get_country_code( $ip_address ) {
            if ( class_exists( 'IP2Location\Database' ) ) {
                $database = __DIR__ . '/vendor/ip2location/ip2location-php/databases/IP-COUNTRY-SAMPLE.BIN';

                if ( file_exists( $database ) ) {
                    $db = new \IP2Location\Database( $database, \IP2Location\Database::FILE_IO );
                    $records = $db->lookup( $ip_address, \IP2Location\Database::ALL );

                    if ( !empty( $records['countryCode'] ) ) {
                        return $records['countryCode'];
                    }
                }
            }
        }
    }
}