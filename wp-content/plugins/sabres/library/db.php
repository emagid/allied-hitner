<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}

/*
 * Copyright 2016 Sabres Security Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


if ( !class_exists( 'SBS_DB' ) ) {
    require_once SABRES_PLUGIN_DIR.'/library/fail.php';

    /**
     * The Sabres Database Class
     *
     * @author Sabres Security inc
     * @package Sabres_Security_Plugin
     * @since 1.0.0
     */
    abstract class SBS_DB {

        public static function get_db_total_size() {
            require_once ABSPATH.'/wp-load.php';
            global $wpdb;
            if (!isset($wpdb))
              SBS_Fail::byeArr(array( 'message'=>"wpdb global is not set",
                                   'code'=>500,
                                   'includeBacktrace'=>true
                                  ));

            @$size = $wpdb->get_var( "SELECT SUM(information_schema.tables.data_length) FROM information_schema.tables WHERE table_schema = '" . DB_NAME . "'" );

            if ( is_wp_error( $size ) ) {
              SBS_Fail::byeArr(array( 'message'=>"Failed to get total db size. ".$size->get_error_message(),
                                   'code'=>500,
                                   'includeBacktrace'=>true,
                                   'logData'=>$size->get_error_data()
                                  ));
              return 0;
            }

            return $size;
        }

        public static function get_current_time() {
            require_once ABSPATH.'/wp-load.php';
            global $wpdb;
            if (!isset($wpdb))
              SBS_Fail::byeArr(array( 'message'=>"wpdb global is not set",
                                   'code'=>500,
                                   'includeBacktrace'=>true
                                  ));

            @$timestamp = $wpdb->get_var( "SELECT NOW()" );

            if ( is_wp_error( $timestamp ) ) {
                SBS_Fail::byeArr(array( 'message'=>"Failed to get timestamp ".$timestamp->get_error_message(),
                                   'code'=>500,
                                   'includeBacktrace'=>true,
                                   'logData'=>$timestamp->get_error_data()
                                  ));
                return 0;
            }

            return strtotime( $timestamp );
        }

        public static function drop_sabres_tables($echo_result) {
          require_once ABSPATH.'/wp-load.php';
          require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
          global $wpdb;
          if (!$echo_result)
            $wpdb->hide_errors();
          else
            $wpdb->show_errors();
          self::drop_sabres_table($wpdb,$echo_result,'sbs_log');
          self::drop_sabres_table($wpdb,$echo_result,'sbs_firewall_cookies');
          self::drop_sabres_table($wpdb,$echo_result,'sbs_firewall_countries');
          self::drop_sabres_table($wpdb,$echo_result,'sbs_firewall_custom');
          self::drop_sabres_table($wpdb,$echo_result,'sbs_scan_items');
          self::drop_sabres_table($wpdb,$echo_result,'sbs_scans');

        }
        private static function drop_sabres_table($wpdb,$echo_result,$table_name) {
          if ($echo_result)
            echo "Dropping $table_name...";
          $sql='drop table IF EXISTS '.$wpdb->prefix.$table_name;
//          $result=dbDelta($sql);
          $result=$wpdb->query($sql);
          if ($echo_result)
            echo var_export($result,true).PHP_EOL;
        }
    }
}
