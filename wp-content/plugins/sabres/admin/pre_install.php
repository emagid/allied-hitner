<?php

if (defined('PRE_INSTALL_INC')) {

  if (!defined('SBS_MAIN_PLUGIN_FILE'))
    throw new Exception("SBS_MAIN_PLUGIN_FILE is undefined", 1);
    if (!defined('SABRES_PLUGIN_DIR'))
     throw new Exception("SABRES_PLUGIN_DIR is undefined", 1);

  class Pre_Install_WP_HOOKS {

    public function activation_hook() {
      @file_put_contents(SABRES_PLUGIN_DIR.'/activation.log',date("Y-m-d H:i:s").' Plugin activated by user (pre-install)'.PHP_EOL,FILE_APPEND | LOCK_EX);   
      $this->activate_pre_install_checklist_page();
    }

    public function register_admin_menu() {

      $customer=$downloadPath=SbrUtils::trgx('name');

      add_menu_page($customer.' - Pre Install Checklist', //page_title
        $customer, //menu title
        'activate_plugins', //capability
        $customer, //menue slug
        array( $this, 'show_pre_install_checklist' ), //function
        plugins_url() . '/sabres/admin/images/logo_dark_16x16.png'
      );
    }



    private function activate_pre_install_checklist_page() {
      set_transient( 'sabres_pre_install_checklist_activated', 1, 30 );
    }

    public function redirect_to_pre_install_checklist_page() {
          // only do this if the user can activate plugins
      if ( ! current_user_can( 'activate_plugins' ) )
      return;

      // don't do anything if the transient isn't set
      if ( ! get_transient( 'sabres_pre_install_checklist_activated' ) )
      return;

      delete_transient( 'sabres_pre_install_checklist_activated' );
      $customer=SbrUtils::trgx('name');
      wp_safe_redirect( admin_url( 'admin.php?page='.$customer.'&go=true'));
      exit;
    }

    public function show_pre_install_checklist() {
      //echo '<div class="wrap"><h2>Welcome to My Awesome Plugin</h2></div>';
      require SABRES_PLUGIN_DIR.'/admin/views/pre_install_view.php';
    }

  }


  $preInstall=new Pre_Install_WP_HOOKS();
  register_activation_hook( SBS_MAIN_PLUGIN_FILE, array( $preInstall, 'activation_hook' ) );
  add_action( 'admin_menu',  array( $preInstall, 'register_admin_menu' ) );
  add_action( 'admin_init', array( $preInstall, 'redirect_to_pre_install_checklist_page' ), 1 );
  add_action( 'wp_ajax_sabres_pre_install', 'Sabres::pre_install');

}
