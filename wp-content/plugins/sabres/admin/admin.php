<?php

class Sabres_Admin {

    protected static $page_postfixes = array(
        'tfa' => '_Tfa'
    );

    public static function init() {
        add_action( 'admin_menu', array( 'Sabres_Admin' , 'admin_menu' ) );

        wp_register_style( 'sabres_admin.css', SBS_PLUGIN_URL . '/admin/styles/style.css', array(), SBS_VERSION );
    		wp_enqueue_style( 'sabres_admin.css');
    		if (is_rtl()) {
            wp_register_style( 'sabres_admin_rtl.css', SBS_PLUGIN_URL . '/admin/styles/style_rtl.css', array(), SBS_VERSION );
            wp_enqueue_style( 'sabres_admin_rtl.css');
    		}

        add_action('admin_footer', array('Sabres_Admin', 'hook_admin_footer'));
    }

    public static function admin_menu() {
      $client_name = SbrUtils::t('name');
        add_menu_page($client_name, $client_name, 'activate_plugins', $client_name, array('Sabres_Admin', 'show_page'), plugins_url() . '/sabres/admin/images/logo_dark_16x16.png');
        add_options_page( __($client_name, 'sabres'), __($client_name, 'sabres'), 'manage_options', 'sabres-key-config', array( 'Sabres_Admin', 'show_page' ) );
        add_submenu_page( null, __($client_name, 'sabres'), null, 'manage_options', static::get_menu_slug( 'tfa' ), array( 'Sabres_Admin', 'show_tfa_options' ) );
    }

    public static function show_page() {
        echo '<h2 class="sbs-header ak-header">' . esc_html__(SbrUtils::t('name'), 'sabres') . '</h2>';

        require_once __DIR__ . '/views/main.php';
    }

    public static function show_tfa_options() {

        $user = wp_get_current_user();

        $errors = array();

        if( !empty($_POST['submit'] ) ) {
            $settings = array(
                'delivery' => @$_POST['delivery'],
                'email'    => @$_POST['email'],
                'smsNumber'    => @$_POST['smsNumber'],
                'strictness' =>@$_POST['strictness'],
                'device-expiry-checked' => @$_POST['device-expiry-checked'],
                'device-expiry-days' =>@$_POST['device-expiry-days']
            );

            $errors = SbrTfa::update_settings( $user, $settings );
        }

        if( count( $errors ) == 0 ) {
            $settings = SbrTfa::get_settings( $user );
        }

        require_once __DIR__ . '/views/tfa.php';
    }

    public static function get_menu_slug($page = null) {
        $client_name = SbrUtils::t('name');

        $page_suffix = isset(static::$page_postfixes[$page]) ? static::$page_postfixes[$page] : '';

        return __($client_name, 'sabres') . $page_suffix;
    }

    public static function delete_cron_jobs() {
      wp_clear_scheduled_hook( 'sabres_daily_cron' );
      $schedule = wp_get_schedule( 'sabres_daily_cron' );

      wp_clear_scheduled_hook( 'sabres_hourly_cron' );
      $schedule = wp_get_schedule( 'sabres_hourly_cron' );

      return $schedule;

    }

    public static function reset_sabres_activation() {
        $echo_results=false;
        $sbs_reset = true;

        $schedule=self::delete_cron_jobs();
        if ( $schedule ) {
            $sbs_reset = false;
        }

        delete_option( 'sbs_version_number' );
        delete_option( 'sbr-settings' );


        if ( get_option( 'sbr-settings' ) ) {
            $sbs_reset = false;
        }
        require_once SABRES_PLUGIN_DIR.'/_inc/settings.php';
        $settings=SbrSettings::instance();
        $settings->reset();
        require_once SABRES_PLUGIN_DIR.'/library/db.php';
        SBS_DB::drop_sabres_tables($echo_results);
        require_once SABRES_PLUGIN_DIR.'/rpc/force_activate_plugin.php';
        Force_Activate_Plugin::activate_plugin_if_necessary($echo_results);

        return $sbs_reset;
    }

    public static function hook_admin_footer() {
        try {
            if (Sabres::get_session_task('attach_account')){
                self::write_attach_account_script();
                Sabres::unset_session_task('attach_account');
            }
        } catch ( \Exception $e ) {
            $error_message = $e->getMessage();

            Sabres::$logger->log( 'error', 'Hook - Sabres_Admin hook_admin_footer', $error_message );
        }
    }

    public static function write_attach_account_script($auto=true) {
        $api_key = Sabres::$settings->apiKey;

        $api_url = Sabres::$server->get_server_api_url() . '/add-website';
        $blog_name = str_replace("'", "\'", get_option('blogname'));
        $site_url = site_url();
        // $site_url = preg_replace('/^https?:\/\//', '', site_url());

        echo <<<EOL
<script type="text/javascript">
jQuery(function($){
    $(document).ready(function() {
EOL;

        $sbr_data = "sbr_ajax_data = {
            apiKey:'$api_key',
            name:'$blog_name',
            url:'$site_url'
        };";

        $ajax_task = <<<EOL
        $.ajax({
            url:'$api_url',
            type:'POST',
            data:sbr_ajax_data,
            xhrFields: {
                withCredentials: true
            },
            success: function(res){
                $('.sabres_activate-status-success').slideDown();
            },
            error: function(){
                $('.sabres_activate-status-fail').slideDown();
            },
            complete: function(){
                $('.sabres-attach-account-spinner.is-active').removeClass('is-active');
                $('form[name=sabres_activate]').slideUp();
            }
        });
EOL;

        if ($auto){
          echo $sbr_data;
          echo $ajax_task;
        } else {
          echo <<<EOL
          $sbr_data
          $('form[name=sabres_activate]').submit(function(e){
            e.preventDefault();
            sbr_ajax_data.name=$(this).find('input[name=name]').val();
            sbr_ajax_data.url=$(this).find('input[name=url]').val();
            sbr_ajax_data.licenseKey=$(this).find('input[name=licenseKey]').val();
            $('.sabres-attach-account-spinner').addClass('is-active').next().attr('disabled','disabled');

            $ajax_task

          });
EOL;
        }

        echo <<<EOL
    });
});
</script>
EOL;
    }

    /*
    Properly encoded URL string
    */
    public static function add_website_info($url){
      $apiKey = Sabres::$settings->apiKey;
      $blog_name = get_option('blogname'); // if i will escape it here it will get escaped twice
      $site_url = site_url();
      // https://www.sabressecurity.com/wp-login.php?action=register
      // https://portal.sabressecurity.com/login/
      // concat to the above...
      $qs = http_build_query(array('website-info' => array('apiKey'=>$apiKey,'name'=>$blog_name,'url'=>$site_url)));
      return $url . ((strpos($url, '?') === FALSE) ? '?' : '&') . $qs;

    }

}
