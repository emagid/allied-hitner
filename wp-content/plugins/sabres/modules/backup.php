<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}

require_once SABRES_PLUGIN_DIR . '/_inc/modules/class.backup.php';