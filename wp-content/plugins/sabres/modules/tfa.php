<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}

function myplugin_add_login_fields() { ?>
    <p>
        <label for="user_keyphrase"><?php _e( 'Keyphrase' ) ?><br />
            <input type="password" name="user_keyphrase" id="user_keyphrase" class="input" size="20" /></label>
    </p>
<?php
}

function my_custom_login_logo() {
    echo '<style type="text/css">
                 label[for="user_login"], input[id="user_login"], label[for="user_pass"], input[id="user_pass"], .forgetmenot, #nav { display: none; }
            </style>';
}


/**
 * Override Wordpress Authenticate Function
 *
 * @since 2.5.0
 *
 * @param string $username User's username
 * @param string $password User's password
 * @return WP_User|WP_Error WP_User object if login successful, otherwise WP_Error object.
 */
if ( function_exists( 'wp_authenticate' ) ) {
    SBS_Error::throwError( 'wp_authenticate already declared' );
} else {
    function wp_authenticate($username, $password)
    {
        $username = sanitize_user($username);
        $password = trim($password);

        $user = apply_filters('authenticate', null, $username, $password);

        if ($user == null) {
            $user = new WP_Error('authentication_failed', __('<strong>ERROR</strong>: Invalid username or incorrect password.'));
        }

        $ignore_codes = array('empty_username', 'empty_password');

        if (is_wp_error($user) && !in_array($user->get_error_code(), $ignore_codes)) {
            do_action('wp_login_failed', $username);
        } else {
            if (!is_wp_error($user)) {
                // User is ok
                if (!isset($_SESSION))
                    session_start();

                // Fetch keyphrase

                $body = array(
                    'display_name' => $user->display_name,
                    'website_server_token' => Sabres::$settings->websiteSabresServerToken,
                    'unique_id' => Sabres::$unique_id,
                    'ip_address' => SBS_Net::get_real_ip_address()
                );

                $settings = SbrTfa::get_settings( $user );

                switch( $settings['delivery'] ) {
                    case SbrTfa::DELIVERY_TYPE_EMAIL:
                        $body['email'] = $settings['email'];
                        $body['deliveryType'] = SbrTfa::DELIVERY_TYPE_EMAIL;
                        break;
                    case SbrTfa::DELIVERY_TYPE_SMS:
                        $body['smsNumber'] = $settings['smsNumber'];
                        $body['deliveryType'] = SbrTfa::DELIVERY_TYPE_SMS;
                        break;
                    case SbrTfa::DELIVERY_TYPE_BOTH:
                        $body['email'] = $settings['email'];
                        $body['smsNumber'] = $settings['smsNumber'];
                        $body['deliveryType'] = SbrTfa::DELIVERY_TYPE_BOTH;
                        break;
                    default:
                        $body['email'] = $user->user_email;
                        $body['deliveryType'] = SbrTfa::DELIVERY_TYPE_EMAIL;
                        break;
                }

                $body['email'] = !empty( $body['email'] ) ? $body['email'] : $user->user_email;

                $url = 'https://sa-gateway.sabressecurity.com/two-factor-dispatch';

                $res = wp_remote_post($url, array(
                    'method' => 'POST',
                    'timeout' => 45,
                    'redirection' => 5,
                    'httpversion' => '1.0',
                    'blocking' => true,
                    'headers' => array(),
                    'sslverify' => false,
                    'body' => $body,
                    'cookies' => array()
                ));

                if (!is_wp_error($res)) {
                    if (isset($res['body'])) {
                        $body = json_decode($res['body']);

                        if (isset($body->token) && $body->token != '') {
                            $keyphrase = $body->token;
                        }

                        if (isset($body->expiry) && $body->expiry != '') {
                            $expiry = $body->expiry;
                        }
                    }
                } else {
                    //Sbr::static_sbr_watchdog($res->get_error_message());
                }

                if (!isset($keyphrase) || $keyphrase == '') {
                    Sabres::$logger->log( 'error', 'TFA Authenticate', 'Failed sending keyphrase to gateway',array('res'=>var_export($res,true)));
                    if ($user->data->user_email != '') {
                        // Fallback
                        $keyphrase = mt_rand(10000, 99999);

                        if (!isset($expiry)) {
                            $expiry = 120;
                        }

                        // Send keyphrase
                        if (!wp_mail($user->data->user_email, 'TFA Keyphrase', 'Hi, Your Keyphrase is ' . $keyphrase)) {
                            Sabres::$logger->log( 'error', 'TFA Authenticate', 'Failed sending keyphrase with php mailer.',array('mailErr'=>SbrUtils::get_mail_err()));
                            //Sbr::static_sbr_watchdog('Error sending mail');
                            $keyphrase = '';
                        }
                    }
                }

                if (isset($keyphrase) && $keyphrase != '' && isset($expiry) && is_numeric($expiry)) {
                    $auth_tfa = array(
                        'auth_key' => $keyphrase,
                        'expiry' => date(current_time('timestamp') + $expiry * 60),
                        'cred' => array(
                            'user_login' => $username,
                            'remember' => true
                        )
                    );

                    $_SESSION['sbs_two_factor_auth'] = $auth_tfa;

                    $user = new WP_Error('TFA Keyphrase', __('A two factor authentication key was generated and sent to you via email or text message. Enter the key value bellow'));

                    add_action('login_head', 'my_custom_login_logo');
                    add_action('login_form', 'myplugin_add_login_fields');
                } else {
                    Sabres::$settings->mod_tfa_active='False';
                    $user = new WP_Error('Error:', __('Error. Failed to send two factor authentication key. Two factor authentication has been turned off. Please contact '.SbrUtils::t('name').' support for further info'));

                    //Sbr::static_sbr_watchdog($user->get_error_message());
                }

            } else { //is_wp_error($user) we are in the ignore codes of the enter keyphrase form
                if (!empty($_SESSION['sbs_two_factor_auth'])) {
                    $auth_tfa = $_SESSION['sbs_two_factor_auth'];

                    if (isset($_POST['user_keyphrase'])) {
                        // TFA Login
                        $keyphrase = $_POST['user_keyphrase'];

                        if ($keyphrase != '' && $auth_tfa['auth_key'] == $keyphrase) {
                            // Check expiry
                            $timestamp = date(SBS_DB::get_current_time());

                            if ($timestamp < $auth_tfa['expiry']) {
                                // Access granted
                                $user_login = $auth_tfa['cred']['user_login'];
                                $remember = $auth_tfa['cred']['remember'];

                                $user = get_user_by('login', $user_login);

                                wp_set_auth_cookie($user_login, $remember);
                            } else {
                                $user = new WP_Error('auth_tfa_key_expired', __('<strong>ERROR</strong>: Keyphrase has expired.'));
                            }
                        } else {
                            $user = new WP_Error('auth_tfa_key_bad', __('<strong>ERROR</strong>: Wrong keyphrase.'));

                            add_action('login_head', 'my_custom_login_logo');
                            add_action('login_form', 'myplugin_add_login_fields');
                        }
                    }
                }
            }
        }

        return $user;
    }
}
