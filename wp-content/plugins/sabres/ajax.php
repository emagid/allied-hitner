<?php


if ( !empty( $_POST['a'] ) ) {
    $action = trim($_POST['a']);

    switch ($action) {
        case 'sc':
            require_once __DIR__ . '/library/include.php';

            SBS_Cache::disable_cache();

            $sabres_config_exists=@include_once SABRES_PLUGIN_DIR.'/sabres-config.php';
            if ($sabres_config_exists)
              $base_path=Sabres_Config::get('ABSPATH');
            else
              $base_path=preg_replace( '/wp-content(?!.*wp-content).*/', '', __DIR__ );
            require_once $base_path . 'wp-load.php';

            break;
    }
}
