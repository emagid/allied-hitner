<?php

define( 'SBS_RPC', true );

// Disable time limit
@ini_set( 'max_execution_time', '0' );
@set_time_limit( 0 );

// Disable memory limit
@ini_set( 'memory_limit', '-1' );

if ($sabres_config_exists)
  $base_path=Sabres_Config::get('ABSPATH');
else
  $base_path=preg_replace( '/wp-content(?!.*wp-content).*/', '', __DIR__ );
require_once $base_path . 'wp-load.php';

if (!class_exists('Sabres')) {
   header('X-PHP-Response-Code: 500', true, 500);
   die('Class Sabres is not defined. Plugin appears to be deactivated.');
}

require_once __DIR__ . '/library/include.php';

SBS_Cache::disable_cache();

try {
    if ( Sabres::$aes_key == '' || Sabres::$aes_iv == '' ) {
        Sabres::$logger->log( 'error', 'RPC', 'Missing encryption keys' );
        Sabres::terminate( 500, 'Missing keys' );
    }

    $res = null;

    $input = file_get_contents( 'php://input' );

    if ( empty( $input ) ) {
        Sabres::terminate( 400, 'Bad Request' );
    }

    $encrypted = true;

    if ( Sabres::$settings->websiteSabresServerToken == '' ) {
        $rpc_data = SBS_Net::parse_url( $input );

        if ( !empty( $rpc_data['op'] ) ) {
            $body = $input;
            $encrypted = false;
        }
    }

    if ( empty( $body ) ) {
        $body = SBS_Crypto::decrypt( Sabres::$aes_key, Sabres::$aes_iv, $input );
        $rpc_data = SBS_Net::parse_url( $body );
    }

    if ( $encrypted ) {
        if ( empty( $rpc_data['sbrendpoint'] ) ) {
            Sabres::$logger->log( 'error', 'RPC', 'Decryption failed' );
            Sabres::terminate( 500, 'Decryption failed', $rpc_data );
        }

        if ( empty( $rpc_data['sig'] ) ) {
            Sabres::$logger->log( 'error', 'RPC', 'Missing signature' );
            Sabres::terminate( 400, 'Missing signature', $rpc_data );
        }

        $sig = trim( $rpc_data['sig'] );
        $rpc_sig = Sabres::generate_signature( $rpc_data );

        if ( $rpc_sig != $sig ) {
            Sabres::$logger->log( 'error', 'RPC', 'Invalid signature' );
            Sabres::terminate( 400, 'Invalid signature', $rpc_data );
        }
    }

    if ( empty( $rpc_data['op'] ) ) {
        Sabres::$logger->log( 'error', 'RPC', 'Missing operation' );
        Sabres::terminate( 400, 'Missing operation', $rpc_data );
    }

    $rpc_action = $rpc_data['op'];

    if ( !$encrypted && !in_array( $rpc_action, array(
            'forceActivate',
            'getLog',
            'getSettings'
        ) )
    ) {
        Sabres::$logger->log( 'error', 'RPC', "Invalid action $rpc_action", $rpc_data );
        Sabres::terminate( 401, "Invalid action $rpc_action" );
    }

    switch ( $rpc_action ) {
        case 'activate':
            if ( empty( $rpc_data['token'] ) ) {
                Sabres::$logger->log( 'error', 'RPC', 'Missing token', $rpc_data );
                Sabres::terminate( 400, 'Missing token' );
            }

            if ( Sabres::$settings->token == '' ) {
                Sabres::$logger->log( 'error', 'RPC', 'Missing token', $rpc_data );
                Sabres::terminate( 500, 'Missing token' );
            }

            if ( Sabres::$settings->token != $rpc_data['token'] ) {
                Sabres::$logger->log( 'error', 'RPC', 'Invalid token', $rpc_data );
                Sabres::terminate( 401, 'Invalid token' );
            }

            if ( empty( $rpc_data['settings'] ) ) {
                Sabres::$logger->log( 'error', 'RPC', 'Missing settings', $rpc_data );
                Sabres::terminate( 400, 'Missing settings' );
            }

            $res = Sabres::activate_plugin( urldecode( $rpc_data['settings'] ) );
            break;
        case 'forceActivate':
            $res = json_encode( Sabres::check_server_token() );
            break;
        case 'getLog':
            $start = null;
            $end = null;

            if ( !empty( $rpc_data['start'] ) && !empty( $rpc_data['end'] ) ) {
                $start = $rpc_data['start'];
                $end = $rpc_data['end'];
            }

            $entries = Sabres::$logger->get_entries( $start, $end );

            $res = json_encode( array_values( $entries ) );
            break;
        case 'getSettings':
            $fields = null;

            if ( !empty( $rpc_data['fields'] ) ) {
                $fields = explode( ',', $rpc_data['fields'] );
            }

            $res = Sabres::get_settings( $fields );
            break;
    }

    if ( !isset( $res ) ) {
        if ( empty( $rpc_data['websiteSabresServerToken'] ) ) {
            Sabres::$logger->log( 'error', 'RPC', 'Missing server token' );
            Sabres::terminate( 400, 'Missing server token' );
        }

        if ( $rpc_data['websiteSabresServerToken'] != Sabres::$settings->websiteSabresServerToken ) {
            Sabres::$logger->log( 'error', 'RPC', 'Invalid server token' );
            Sabres::terminate( 401, 'Invalid server token' );
        }

        switch ( $rpc_action ) {
            case 'dirIt':
                $res = json_encode( array_values( Sabres::get_files_info() ) );
                break;
            case 'clearLog':
                $start = null;
                $end = null;

                if ( !empty( $rpc_data['start'] ) && !empty( $rpc_data['end'] ) ) {
                    $start = $rpc_data['start'];
                    $end = $rpc_data['end'];
                }

                $res = json_encode( Sabres::$logger->clear_entries( $start, $end ) );
                break;
            case 'settings':
                if ( empty( $rpc_data['settings'] ) ) {
                    Sabres::$logger->log( 'error', 'RPC', 'Missing settings' );
                    Sabres::terminate( 400, 'Missing settings' );
                }

                $res = Sabres::set_settings( urldecode( $rpc_data['settings'] ) );
                break;
            case 'coreInventory':
                $files = null;

                if ( !empty( $rpc_data['files'] ) ) {
                    $files = $rpc_data['files'];
                }

                $res = json_encode( Sabres::get_core( $files ) );
                break;
            case 'pluginsInventory':
                $files = null;

                if ( !empty( $rpc_data['files'] ) ) {
                    $files = $rpc_data['files'];
                }

                $res = json_encode( Sabres::get_plugins( $files ) );
                break;
            case 'themesInventory':
                $files = null;

                if ( !empty( $rpc_data['files'] ) ) {
                    $files = $rpc_data['files'];
                }

                $res = json_encode( Sabres::get_themes( $files ) );
                break;
            case 'themesInventory2':
                $files = null;

                if ( !empty( $rpc_data['files'] ) ) {
                    $files = $rpc_data['files'];
                }

                $res = json_encode( Sabres::get_themes_2( $files ) );
                break;
            case 'updatePlugin':
                $filename = null;

                if ( isset( $rpc_data['fileName'] ) && $rpc_data['fileName'] != '' ) {
                    $filename = $rpc_data['fileName'];
                }

                $res = Sabres::update_plugin( $filename );
                break;
            case 'firewallSettings':
                require_once SABRES_PLUGIN_DIR . '/_inc/modules/class.firewall.php';
                $topic = null;
                $data = null;
                $purge = null;

                if ( !empty( $rpc_data['topic'] ) ) {
                    $topic = $rpc_data['topic'];
                }
                if ( !empty( $rpc_data['body'] ) ) {
                    $data = urldecode( $rpc_data['body'] );
                }
                if ( !empty( $rpc_data['purge'] ) ) {
                    $purge = $rpc_data['purge'];
                }

                $res = Sabres::firewall_settings( $topic, $data, $purge );
                break;
            case 'performMalwareScan':
                $scanner = SBS_Scanner::getInstance();
                $scanner->init( Sabres::$settings->get_settings( 'mod_scanner' ), 'malware' );
                $scanner->run();

                $scan = $scanner->get_current_scan();

                if ( isset( $scan ) ) {
                    $scan_items = array();

                    if ( !empty( $scan->items ) ) {
                        $scan_items = array_map( function ( $item ) {
                            unset( $item->ID );
                            unset( $item->parent_id );

                            return $item;
                        }, $scan->items );
                    }

                    $res = json_encode( array_values( $scan_items ) );
                }
                break;
            case 'performSecurityScan':
                $scanner = SBS_Scanner::getInstance();
                $scanner->init( Sabres::$settings->get_settings( 'mod_scanner' ), 'security' );
                $scanner->run();

                $scan = $scanner->get_current_scan();

                if ( isset( $scan ) ) {
                    $scan_items = array();

                    if ( !empty( $scan->items ) ) {
                        $scan_items = array_map( function ( $item ) {
                            unset( $item->ID );
                            unset( $item->parent_id );

                            return $item;
                        }, $scan->items );
                    }

                    $res = json_encode( array_values( $scan_items ) );
                }
                break;
            case 'performSecurityFix':
                $scanner = SBS_Scanner::getInstance();
                $scanner->init( Sabres::$settings->get_settings( 'mod_scanner' ), 'security' );

                $res = json_encode( $scanner->fix_issue( $rpc_data['code'], $rpc_data['uniqueId'] ) );
                break;
            case 'getScans':
                $scanner = SBS_Malware_Scanner::getInstance();
                $scans = $scanner->get_scans_by_status( 'running' );

                if ( !empty( $scans ) ) {
                    $res = json_encode( array_values( $scans ) );
                }
                break;
            case 'backup':
                require_once __DIR__ . '/_inc/modules/class.backup.db.php';

                $engine = new SBS_Backup_DB_Engine();
                $engine->init( array(), 'sbs_backup' );
                $engine->run();

                SBS_Zip::archive( ABSPATH, WP_CONTENT_DIR . '/sbs-backup/sbs_backup.zip' );
                break;
            case 'uploadBackup':
                $access_token = $rpc_data['access_token'];

                require_once __DIR__ . '/_inc/modules/class.backup.gDrive.php';

                $client_id = $rpc_data['client_id'];
                $client_secret = $rpc_data['client_secret'];

                $google_drive = new SBS_Backup_G_Drive();
                $google_drive->init( array(), 'sbs_backup', $client_id, $client_secret );

                $google_drive->run( $access_token );
                break;
            case 'getVersion':
                $res = json_encode( array(
                    'version' => SBS_VERSION,
                    'dbVersion' => SBS_DB_VERSION
                ) );
                break;
            case 'getSSLInfo':
                $info = Sabres::get_ssl_info();

                $res = json_encode( $info );
                break;
            case 'getSystemInfo':
                $info = Sabres::get_system_info();
                $info = SBS_Array::array_utf8_encode($info);

                $res = json_encode( $info );
                break;
            default:
                Sabres::$logger->log( 'error', 'RPC', "Invalid action $rpc_action" );
                Sabres::terminate( 400, "Invalid action $rpc_action" );
                break;
        }
    }

    if ( !empty( $res ) ) {
        if ( $encrypted ) {
            header( 'Content-Type: text/plain' );

            echo SBS_Crypto::encrypt( Sabres::$aes_key, Sabres::$aes_iv, $res );
        } else {
            header( 'Content-Type: application/json' );

            echo $res;
        }
    } else {
        header( 'Content-Type: application/json' );
    }

    Sabres::$logger->log( 'info', 'RPC', "Action: $rpc_action", '' );
} catch ( \Exception $e ) {
    $error_message = $e->getMessage();

    Sabres::$logger->log( 'error', 'RPC', "$rpc_action - $error_message" );
    Sabres::terminate( 500, $e->getMessage() );
}

exit();
